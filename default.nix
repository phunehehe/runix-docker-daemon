{ exports, home, lib, pkgs, ... }:
with builtins;
let
  group = "docker";
  users = lib.concatStringsSep "," (lib.concatLists (map (value:
    value.docker-daemon.users or []
  ) (lib.attrValues exports)));
  cgroupfs-mount = pkgs.fetchFromGitHub {
    owner = "tianon";
    repo = "cgroupfs-mount";
    sha256 = "1hyxhhr6yybz08pfs77p4f6pjckdsc2b6vqwi7hvv5fnsabf6cnc";
    rev = "1.3";
  };
in {
  exports = { inherit group; };
  run = pkgs.writeBash (baseNameOf ./.) ''

    ${pkgs.glibc.bin}/bin/getent group ${group} \
    || ${pkgs.shadow}/bin/groupadd --system ${group}

    ${pkgs.shadow}/bin/gpasswd --members ${users} ${group}

    PATH=${lib.makeBinPath [
      pkgs.coreutils
      pkgs.gawk
      pkgs.gnugrep
      pkgs.utillinux # mount
    ]} ${cgroupfs-mount}/cgroupfs-mount

    exec ${pkgs.coreutils}/bin/env PATH=${lib.makeBinPath [
      pkgs.kmod # modprobe
    ]} ${pkgs.docker}/bin/docker daemon \
      --exec-root=${home} \
      --graph=${home}/graph
  '';
}
